FROM node:latest as build-deps

# set the working direction
WORKDIR /app

# install app dependencies
COPY . ./

RUN npm install -g npm@7.8.0 && \
    npm install && \
    npm run build

# start app
CMD ["npm", "start"]

FROM nginx:1.15.2-alpine
# Copy the build folder of the react app
COPY --from=build-deps /app/build /var/www
# Copy the ngnix configrations
COPY --from=build-deps /app/nginx.conf /etc/nginx/nginx.conf
# Expose it on port 80
EXPOSE 80
ENTRYPOINT ["nginx","-g","daemon off;"]